package tests;

import org.junit.Rule;
import org.junit.rules.Timeout;
import tests.model.Installment;
import tests.model.Order;
import tests.model.Result;
import tests.model.ValidationErrors;
import util.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertFalse;

public abstract class AbstractHw {

    @Rule
    public Timeout globalTimeout = Timeout.seconds(20);

    protected abstract String getBaseUrl();

    private static boolean isLoggingEnabled = false;

    protected static String frameworkPathToSourceCode = "";

    protected static String arg1 = "";

    protected static void enableLogging() {
        isLoggingEnabled = true;
    }

    public static void setPathToSourceCode(String path) {
        frameworkPathToSourceCode = path;
    }

    public static void setArg1(String value) {
        arg1 = value;
    }

    protected void assertProjectSourcePathIsSet(String path) {
        if (!frameworkPathToSourceCode.isEmpty()) {
            return;
        }

        assertFalse("Please assign the path to your project source code directory " +
                        "to the field 'pathToProjectSourceCode'",
                path == null || path.isEmpty());
    }

    protected void assertDoesNotContainString(List<FileReader.File> files,
                                            String targetString) {

        for (FileReader.File file : files) {
            String message = MessageFormat.format(
                    "file {0} contains string ''{1}''",
                    file.name, targetString);

            assertThat(message, file.contents, not(containsString(targetString)));
        }
    }

    protected List<FileReader.File> getProjectSource(String pathToProjectSourceCode) {
        String path = frameworkPathToSourceCode.isEmpty()
                ? pathToProjectSourceCode
                : frameworkPathToSourceCode;

        return new FileReader().getAllFilesAndContentsFrom(
                Paths.get(path), List.of(".java"));
    }

    private static Client getClient() {
        try {
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[] {new NopX509TrustManager()}, new SecureRandom());
            return ClientBuilder.newBuilder()
                    .register(new LoggingFilter(isLoggingEnabled))
                    .register(JacksonConfig.class)
                    .register(ContentTypeFilter.class)
                    .sslContext(sslcontext).hostnameVerifier((s1, s2) -> true).build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public WebTarget getTarget() {
        return getClient().target(getBaseUrl());
    }

    private <T> List<T> getList(String path,
                                GenericType<List<T>> type,
                                Parameter... parameters) {

        WebTarget target = getTarget().path(path);

        for (Parameter p : parameters) {
            target = target.queryParam(p.getKey(), p.getValue());
        }

        return target
                .request(MediaType.APPLICATION_JSON)
                .get(type);
    }

    protected List<Installment> getInstallmentList(String path, Parameter... parameters) {
        return getList(path, new GenericType<List<Installment>>() {}, parameters);
    }

    protected List<Order> getList(String path, Parameter... parameters) {
        return getList(path, new GenericType<List<Order>>() {}, parameters);
    }

    protected Order getOne(String path, Parameter... parameters) {
        return getOne(path, Order.class, parameters);
    }

    protected String getResponseAsString(String path) {
        return getOne(path, String.class, new Parameter[] {});
    }

    protected <T> T getOne(String path, Class<T> clazz, Parameter ... parameters) {
        WebTarget target = getTarget().path(path);

        for (Parameter p : parameters) {
            target = target.queryParam(p.getKey(), p.getValue());
        }

        return target
                .request(MediaType.APPLICATION_JSON)
                .get(clazz);
    }

    protected Parameter param(String key, Object value) {
        return new Parameter(key, String.valueOf(value));
    }

    protected Result<Order> postOrder(String path, Order data) {
        Response response = getTarget()
                .path(path)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(data, MediaType.APPLICATION_JSON));

        Result<Order> result = new Result<>();

        if (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode()) {
            result.setErrors(response.readEntity(ValidationErrors.class).getErrors());
        } else {
            result.setValue(response.readEntity(Order.class));
        }

        return result;
    }

    protected Result<Map<String, Object>> postMap(
            String path,
            Map<String, Object> data) {

        Response response = getTarget()
                .path(path)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(data, MediaType.APPLICATION_JSON));

        Result<Map<String, Object>> result = new Result<>();

        if (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode()) {
            result.setErrors(response.readEntity(ValidationErrors.class).getErrors());
        } else {
            result.setValue(response.readEntity(
                    new GenericType<Map<String, Object>>() {}));
        }

        return result;
    }

    protected boolean sendRequest(String path) {
        Response response = getClient()
                .target(path)
                .request()
                .header("x-is-base-url-request", "true")
                .get();

        return Response.Status.OK.getStatusCode() == response.getStatus();
    }

    protected Result<Order> postOrderFromJsonString(String path, String data) {
        Response response = getTarget()
                .path(path)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(data, MediaType.APPLICATION_JSON));

        Result<Order> result = new Result<>();

        if (response.getStatus() == Response.Status.BAD_REQUEST.getStatusCode()) {
            result.setErrors(response.readEntity(ValidationErrors.class).getErrors());
        } else {
            result.setValue(response.readEntity(Order.class));
        }

        return result;
    }

    protected void delete(String path, Parameter ... parameters) {
        WebTarget target = getTarget().path(path);

        for (Parameter p : parameters) {
            target = target.queryParam(p.getKey(), p.getValue());
        }

        target.request().delete();
    }

}
